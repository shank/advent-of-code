use anyhow::anyhow;
use anyhow::{Context, Result};

use std::str::FromStr;

fn main() -> Result<()> {
    // let input = std::fs::read_to_string("input/day12_test.in")
    let input = std::fs::read_to_string("input/day12.in")
        .with_context(|| format!("error reading input file"))?;

    let commands = parse_input(&input).with_context(|| format!("error parsing input"))?;
    let ans1 = part1(&commands);
    println!("part1: {}", ans1);

    let ans2 = part2(&commands);
    println!("part2: {}", ans2);

    Ok(())
}

fn part2(commands: &[Cmd]) -> u64 {
    let mut ship = Ship::new();
    for command in commands {
        ship.exec_cmd2(*command);
    }

    return (ship.x.abs() + ship.y.abs()) as u64;
}

fn part1(commands: &[Cmd]) -> u64 {
    let mut ship = Ship::new();
    for command in commands {
        ship.exec_cmd1(*command);
    }

    return (ship.x.abs() + ship.y.abs()) as u64;
}

#[derive(Debug)]
struct Ship {
    x: i64,
    y: i64,
    direction: Direction,
    wx: i64,
    wy: i64,
}

impl Ship {
    fn new() -> Self {
        Self {
            x: 0,
            y: 0,
            direction: Direction::East,
            wx: 10,
            wy: 1,
        }
    }

    fn exec_cmd1(&mut self, cmd: Cmd) {
        match cmd {
            Cmd::North(d) => self.y += d as i64,
            Cmd::South(d) => self.y -= d as i64,
            Cmd::East(d) => self.x += d as i64,
            Cmd::West(d) => self.x -= d as i64,
            Cmd::Left(d) => self.direction = self.direction.left(d),
            Cmd::Right(d) => self.direction = self.direction.right(d),
            Cmd::Forward(d) => match self.direction {
                Direction::East => self.exec_cmd1(Cmd::East(d)),
                Direction::West => self.exec_cmd1(Cmd::West(d)),
                Direction::North => self.exec_cmd1(Cmd::North(d)),
                Direction::South => self.exec_cmd1(Cmd::South(d)),
            },
        }
    }

    fn exec_cmd2(&mut self, cmd: Cmd) {
        match cmd {
            Cmd::North(d) => self.wy += d as i64,
            Cmd::South(d) => self.wy -= d as i64,
            Cmd::East(d) => self.wx += d as i64,
            Cmd::West(d) => self.wx -= d as i64,
            Cmd::Left(d) => match d % 360 {
                0 => {}
                90 => {
                    let tmp = self.wx;
                    self.wx = -self.wy;
                    self.wy = tmp;
                }
                180 => {
                    self.exec_cmd2(Cmd::Left(90));
                    self.exec_cmd2(Cmd::Left(90));
                }
                270 => {
                    self.exec_cmd2(Cmd::Left(90));
                    self.exec_cmd2(Cmd::Left(180));
                }
                _ => unreachable!(),
            },
            Cmd::Right(d) => self.exec_cmd2(Cmd::Left(360 - d)),
            Cmd::Forward(d) => {
                self.x += d as i64 * self.wx;
                self.y += d as i64 * self.wy;
            }
        }
    }
}

#[derive(Debug, PartialEq)]
enum Direction {
    North,
    South,
    East,
    West,
}

impl Direction {
    fn right(&self, angle: u64) -> Self {
        let angle = angle % 360;
        self.left(360 - angle)
    }

    fn left(&self, angle: u64) -> Self {
        let angle = angle % 360;
        match self {
            Direction::East => match angle {
                0 => Direction::East,
                90 => Direction::North,
                180 => Direction::West,
                270 => Direction::South,
                _ => unreachable!(),
            },
            Direction::North => Direction::East.left(90 + angle),
            Direction::West => Direction::East.left(180 + angle),
            Direction::South => Direction::East.left(270 + angle),
        }
    }
}

#[test]
fn test_direction() {
    assert_eq!(Direction::North.left(90), Direction::West);
    assert_eq!(Direction::North.right(90), Direction::East);
    assert_eq!(Direction::South.left(90), Direction::East);
    assert_eq!(Direction::South.right(90), Direction::West);
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum Cmd {
    North(u64),
    South(u64),
    East(u64),
    West(u64),
    Left(u64),
    Right(u64),
    Forward(u64),
}

impl FromStr for Cmd {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let c = s.chars().nth(0).expect("empty command");
        let d: u64 = s[1..s.len()]
            .parse()
            .with_context(|| format!("failed to parse distance from command: {}", s))?;

        match c {
            'N' => Ok(Cmd::North(d)),
            'S' => Ok(Cmd::South(d)),
            'E' => Ok(Cmd::East(d)),
            'W' => Ok(Cmd::West(d)),
            'L' => Ok(Cmd::Left(d)),
            'R' => Ok(Cmd::Right(d)),
            'F' => Ok(Cmd::Forward(d)),
            _ => Err(anyhow!("unknown command character: {}", c)),
        }
    }
}

fn parse_input(input: &str) -> Result<Vec<Cmd>> {
    input
        .lines()
        .map(|l| {
            l.parse()
                .with_context(|| format!("could not parse command: {}", l))
        })
        .collect()
}
