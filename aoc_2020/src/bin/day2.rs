use std::{fs::File, io::BufRead, io::BufReader};

fn part1(data: Vec<String>) -> usize {
    // 1-3 a: abcde
    // 1-3 b: cdefg
    // 2-9 c: ccccccccc

    let mut valid = 0;

    for line in data {
        let parts = line.split(' ').collect::<Vec<_>>();

        let p1 = parts[0].split('-').collect::<Vec<_>>();
        let min = p1[0].parse::<usize>().unwrap();
        let max = p1[1].parse::<usize>().unwrap();

        let c = parts[1].chars().next().unwrap();

        let password = parts[2];
        let counts = password.chars().filter(|&ch| ch == c).count();
        if min <= counts && counts <= max {
            valid += 1;
        }
    }

    valid
}

fn part2(data: Vec<String>) -> usize {
    // 1-3 a: abcde
    // 1-3 b: cdefg
    // 2-9 c: ccccccccc

    let mut valid = 0;

    for line in data {
        let parts: Vec<_> = line.split(' ').collect();

        let p1: Vec<_> = parts[0].split('-').collect();
        let idx1: usize = p1[0].parse().unwrap();
        let idx2: usize = p1[1].parse().unwrap();

        let c = parts[1].chars().next().unwrap();

        let password = parts[2];
        let test1 = password.chars().nth(idx1 - 1).unwrap().eq(&c);
        let test2 = password.chars().nth(idx2 - 1).unwrap().eq(&c);
        if (test1 && !test2) || (!test1 && test2) {
            valid += 1;
        }
    }

    valid
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let name = aoc2020::get_exec_name().ok_or("invalid binary name")?;

    let input1 = format!("input/{}_1.in", &name);
    let input1 = BufReader::new(File::open(input1)?)
        .lines()
        .collect::<Result<Vec<String>, _>>()?;

    println!("part 1: {}", part1(input1));

    let input2 = format!("input/{}_2.in", &name);
    let input2 = BufReader::new(File::open(input2)?)
        .lines()
        .collect::<Result<Vec<String>, _>>()?;

    println!("part 2: {}", part2(input2));

    Ok(())
}
