use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn part1(data: &Vec<Vec<char>>, slope: (usize, usize)) -> usize {
    let mut x = 0;
    let mut y = 0;
    let (dx, dy) = slope;
    let cols = data[0].len();

    let mut trees = if data[0][0] == '#' { 1 } else { 0 };
    loop {
        y += dy;
        if y < data.len() {
            x = (x + dx) % cols;
            if data[y][x] == '#' {
                trees += 1;
            }
        } else {
            break;
        }
    }

    trees
}

fn part2(data: &Vec<Vec<char>>, slopes: Vec<(usize, usize)>) -> usize {
    let mut result = 1;
    for slope in slopes {
        let trees = part1(&data, slope);
        result *= trees;
    }
    result
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let day = aoc2020::get_exec_name().unwrap();

    let input1 = File::open(format!("input/{}_1.in", day))?;
    let input1 = BufReader::new(input1)
        .lines()
        .collect::<Result<Vec<_>, _>>()?
        .iter()
        .map(|line| line.chars().collect())
        .collect::<Vec<_>>();

    println!("part 1: {}", part1(&input1, (3, 1)));

    let slopes = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    println!("part 2: {}", part2(&input1, slopes));

    Ok(())
}
