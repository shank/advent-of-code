use std::collections::HashMap;
use std::collections::HashSet;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input = std::fs::read_to_string("input/day7.in")?;

    let mut outers: HashMap<String, HashSet<String>> = HashMap::new();
    let mut inners: HashMap<String, Vec<(String, usize)>> = HashMap::new();
    for line in input.split("\n").filter(|s| !s.is_empty()) {
        let (outer, inner_bags) = parse_line(line);
        for (inner, _count) in &inner_bags {
            let entry = outers
                .entry(inner.clone())
                .or_insert_with(|| HashSet::new());
            entry.insert(outer.clone());
        }
        inners.insert(outer, inner_bags.iter().cloned().collect());
    }

    let mut result_set: HashSet<String> = HashSet::new();
    let mut to_check: Vec<String> = outers
        .get(&String::from("shiny gold"))
        .unwrap()
        .iter()
        .cloned()
        .collect();
    let mut checked: HashSet<String> = HashSet::new();
    while !to_check.is_empty() {
        let outer = to_check.pop().unwrap();
        if checked.contains(&outer) {
            continue;
        }
        checked.insert(outer.clone());
        if let Some(parents) = outers.get(&outer) {
            to_check.extend(parents.iter().cloned());
        }
        result_set.insert(outer.clone());
    }

    println!("part1: {}", result_set.len());

    println!(
        "part2: {}",
        count_inners(&inners, &String::from("shiny gold"))
    );

    Ok(())
}

fn count_inners(data: &HashMap<String, Vec<(String, usize)>>, root: &String) -> usize {
    match data.get(root) {
        Some(ref inners) => {
            let mut result = 0;
            for (inner, count) in inners.iter() {
                result += count;
                result += count * count_inners(data, inner)
            }
            result
        }
        None => 0,
    }
}

fn parse_line(line: &str) -> (String, Vec<(String, usize)>) {
    let line = line
        .trim_end_matches(".")
        .replace("no other bags", "")
        .replace(" bags", "")
        .replace(" bag", "");
    let mut parts = line.split(" contain ");
    let bag_colour = parts.next().unwrap();
    let rest = parts.next().unwrap();
    let parts = rest
        .split(", ")
        .filter(|s| !s.is_empty())
        .map(|s| {
            let mut iter = s.split(' ');
            let count: usize = iter.next().unwrap().parse().unwrap();
            (iter.collect::<Vec<_>>().join(" "), count)
        })
        .collect();
    return (bag_colour.to_owned(), parts);
}

#[test]
fn test_parsing() {
    let checks = &[
        (
            "light red bags contain 1 bright white bag, 2 muted yellow bags.",
            (
                String::from("light red"),
                vec![
                    (String::from("bright white"), 1),
                    (String::from("muted yellow"), 2),
                ],
            ),
        ),
        (
            "dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
            (
                String::from("dark orange"),
                vec![
                    (String::from("bright white"), 3),
                    (String::from("muted yellow"), 4),
                ],
            ),
        ),
        (
            "bright white bags contain 1 shiny gold bag.",
            (
                String::from("bright white"),
                vec![(String::from("shiny gold"), 1)],
            ),
        ),
        (
            "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
            (
                String::from("muted yellow"),
                vec![
                    (String::from("shiny gold"), 2),
                    (String::from("faded blue"), 9),
                ],
            ),
        ),
        (
            "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
            (
                String::from("shiny gold"),
                vec![
                    (String::from("dark olive"), 1),
                    (String::from("vibrant plum"), 2),
                ],
            ),
        ),
        (
            "dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
            (
                String::from("dark olive"),
                vec![
                    (String::from("faded blue"), 3),
                    (String::from("dotted black"), 4),
                ],
            ),
        ),
        (
            "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
            (
                String::from("vibrant plum"),
                vec![
                    (String::from("faded blue"), 5),
                    (String::from("dotted black"), 6),
                ],
            ),
        ),
        (
            "faded blue bags contain no other bags.",
            (String::from("faded blue"), vec![]),
        ),
        (
            "dotted black bags contain no other bags.",
            (String::from("dotted black"), vec![]),
        ),
    ];

    for (line, correct) in checks {
        let result = parse_line(line);
        assert_eq!(*correct, result);
    }
}
