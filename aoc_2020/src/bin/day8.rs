use std::{collections::HashSet, str::FromStr};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // let input = std::fs::read_to_string("input/day8_test.in")?;
    let input = std::fs::read_to_string("input/day8.in")?;

    let instructions: Vec<Inst> = input
        .split("\n")
        .filter(|line| !line.is_empty())
        .map(|s| s.parse::<Inst>().unwrap())
        .collect();

    let mut m = Machine::new();
    let (_, acc) = m.run(&instructions);
    println!("part1: {}", acc);

    let acc2 = instructions
        .iter()
        .enumerate()
        .filter_map(|(i, inst)| match inst {
            Inst::Jmp(_) => Some(i),
            _ => None,
        })
        .find_map(|idx| {
            let mut instructions = instructions.clone();
            instructions[idx] = Inst::Nop;
            match m.run(&instructions) {
                (Termination::Success, acc) => Some(acc),
                (Termination::InfiniteLoop, _) => None,
            }
        })
        .expect("no possible result for part2");
    println!("part2: {}", acc2);

    Ok(())
}

#[derive(Debug)]
struct Machine {
    inst_ptr: i64,
    acc: i64,
}

impl Machine {
    fn new() -> Self {
        Self {
            inst_ptr: 0,
            acc: 0,
        }
    }

    fn run(&mut self, program: &Vec<Inst>) -> (Termination, i64) {
        self.acc = 0;
        self.inst_ptr = 0;

        let mut visited: HashSet<i64> = HashSet::new();
        loop {
            if visited.contains(&self.inst_ptr) {
                return (Termination::InfiniteLoop, self.acc);
            }

            if self.inst_ptr == program.len() as i64 {
                return (Termination::Success, self.acc);
            }

            visited.insert(self.inst_ptr);
            let inst = program[self.inst_ptr as usize];
            match inst {
                Inst::Nop => self.inst_ptr += 1,
                Inst::Acc(arg) => {
                    self.inst_ptr += 1;
                    self.acc += arg;
                }
                Inst::Jmp(arg) => {
                    self.inst_ptr += arg;
                }
            }
        }
    }
}

#[derive(Debug)]
enum Termination {
    InfiniteLoop,
    Success,
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum Inst {
    Nop,
    Acc(i64),
    Jmp(i64),
}

impl FromStr for Inst {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.trim().split(' ');
        let op = parts.next().unwrap();
        let arg = parts
            .next()
            .unwrap()
            .parse::<i64>()
            .map_err(|_| "error parsing arg".to_string())?;

        let inst = match op {
            "nop" => Inst::Nop,
            "acc" => Inst::Acc(arg),
            "jmp" => Inst::Jmp(arg),
            _ => return Err(format!("unknown instruction {}", op)),
        };
        Ok(inst)
    }
}

#[test]
fn test_parse_inst() {
    let checks = &[
        ("nop +0", Inst::Nop),
        ("acc +1", Inst::Acc(1)),
        ("jmp +4", Inst::Jmp(4)),
    ];

    for check in checks {
        assert_eq!(check.0.parse::<Inst>(), Ok(check.1));
    }
}
