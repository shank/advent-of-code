use anyhow::{Context, Result};
use std::collections::HashSet;
use std::ops::RangeInclusive;
use std::str::FromStr;

fn main() -> Result<()> {
    let input = parse_input()?;

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

type Ticket = Vec<i64>;
type Field = (String, Vec<RangeInclusive<i64>>);

#[derive(Debug)]
struct Input {
    fields: Vec<(String, Vec<RangeInclusive<i64>>)>,
    your_ticket: Ticket,
    nearby_tickets: Vec<Ticket>,
}

impl FromStr for Input {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split("\n\n");

        // fields section
        let fields_section = parts
            .next()
            .with_context(|| format!("fields section not found"))?;
        let fields = parse_fields_section(fields_section)?;

        // your_ticket section
        let your_ticket_section = parts
            .next()
            .with_context(|| format!("your_ticket section not found"))?;
        let mut tickets = parse_tickets_section(your_ticket_section)?;
        let your_ticket = tickets
            .pop()
            .with_context(|| format!("failed to parse your_ticket section"))?;

        // nearby_tickets section
        let nearby_tickets_section = parts
            .next()
            .with_context(|| format!("nearby_tickets section not found"))?;
        let nearby_tickets = parse_tickets_section(nearby_tickets_section)?;

        Ok(Input {
            fields,
            your_ticket,
            nearby_tickets,
        })
    }
}

fn parse_fields_section(s: &str) -> Result<Vec<Field>> {
    let mut fields = vec![];
    for line in s.lines() {
        fields.push(parse_field_line(line)?);
    }
    Ok(fields)
}

#[test]
fn test_parse_fields_section() {
    let s = r#"class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50"#;

    let fields = parse_fields_section(s).unwrap();
    assert!(fields.len() == 3);
    assert_eq!(fields[0], ("class".to_owned(), vec![1..=3, 5..=7]));
    assert_eq!(fields[1], ("row".to_owned(), vec![6..=11, 33..=44]));
    assert_eq!(fields[2], ("seat".to_owned(), vec![13..=40, 45..=50]));
}

fn parse_field_line(s: &str) -> Result<Field> {
    // departure location: 32-174 or 190-967
    let mut line_parts = s.split(": ");

    let class = line_parts
        .next()
        .with_context(|| format!("field name not found"))?
        .to_owned();
    let mut field_ranges = vec![];
    for part in line_parts.next().unwrap().split(' ') {
        if part.eq("or") {
            continue;
        }
        let range_parts = part
            .split('-')
            .map(|n| n.parse::<i64>().unwrap())
            .collect::<Vec<i64>>();
        let range = range_parts[0]..=range_parts[1];
        field_ranges.push(range);
    }

    Ok((class, field_ranges))
}

#[test]
fn test_parse_field_line() {
    let s = "departure location: 32-174 or 190-967";
    let field = parse_field_line(&s).unwrap();
    assert_eq!(
        field,
        ("departure location".to_owned(), vec![32..=174, 190..=967])
    );
}

fn parse_tickets_section(s: &str) -> Result<Vec<Ticket>> {
    let mut lines = s.lines();
    let mut tickets = vec![];

    lines.next(); // skip the first line
    for line in lines {
        let ticket = line
            .split(',')
            .map(|n| n.parse::<i64>().unwrap())
            .collect::<Vec<_>>();
        tickets.push(ticket);
    }

    Ok(tickets)
}

#[test]
fn test_parse_tickets_section() {
    let s = r#"your ticket:
7,1,14"#;
    let ticket = parse_tickets_section(s).unwrap();
    assert_eq!(ticket, vec![vec![7, 1, 14]]);
}

fn parse_input() -> Result<Input> {
    // std::fs::read_to_string("input/day16_test.in")?.parse()
    std::fs::read_to_string("input/day16.in")?.parse()
}

#[test]
fn test_part1() {
    let input = read_test_input().unwrap();
    assert_eq!(part1(&input).ok(), Some(71));
}

fn is_valid(ticket: &Vec<i64>, fields: &Vec<Field>) -> bool {
    let mut valid = true;
    for n in ticket {
        let mut valid_n = false;
        for f in fields {
            if f.1.iter().any(|r| r.contains(n)) {
                valid_n = true;
                break;
            }
        }
        if !valid_n {
            valid = false;
            break;
        }
    }
    valid
}

#[test]
fn test_is_valid() {
    let input = read_test_input().unwrap();
    assert!(is_valid(&input.your_ticket, &input.fields));
    assert!(is_valid(&input.nearby_tickets[0], &input.fields));
    assert!(!is_valid(&input.nearby_tickets[1], &input.fields));
    assert!(!is_valid(&input.nearby_tickets[2], &input.fields));
    assert!(!is_valid(&input.nearby_tickets[3], &input.fields));
}

fn part1(input: &Input) -> Result<i64> {
    let result = input
        .nearby_tickets
        .iter()
        .map(|t| {
            // println!("ticket: {:?}", t);
            let mut sum: i64 = 0;
            for n in t {
                let mut invalid = true;
                for field in &input.fields {
                    if field.1.iter().any(|r| r.contains(n)) {
                        invalid = false;
                        break;
                    }
                }
                if invalid {
                    // println!("invalid n: {}", n);
                    sum += n;
                }
            }
            sum
        })
        .sum();

    Ok(result)
}

fn part2(input: &Input) -> Result<i64> {
    // find out all the possible fields for each position
    // find all the valid permutations that satisfy this condition.
    // return the first such permutation

    let probables: Vec<Vec<usize>> = (0..input.your_ticket.len())
        .map(|pos| probable_fields_for_pos(pos, input))
        .collect(); // probable fields for each position

    let ordering = find_field_positions(&probables, input);

    let result = ordering
        .iter()
        .zip(input.your_ticket.iter())
        .filter(|(&idx, _)| idx <= 5)
        .map(|(_, n)| *n)
        .product();

    Ok(result)
}

fn find_field_positions(probables: &Vec<Vec<usize>>, input: &Input) -> Vec<usize> {
    let mut result = vec![];
    helper(probables, input, 0, Vec::new().as_mut(), &mut result);
    // result is the ordering of field indices as per ticket field
    result[0].clone()
}

fn helper(
    probables: &Vec<Vec<usize>>, // input: probables
    input: &Input,
    at: usize,                    // the current ticket field idx
    curr: &mut Vec<usize>,        // the current solution
    result: &mut Vec<Vec<usize>>, // the collected results
) {
    if at == input.your_ticket.len() {
        result.push(curr.clone());
    } else {
        for idx in &probables[at] {
            if curr.contains(idx) {
                continue;
            }
            curr.push(*idx);
            helper(probables, input, at + 1, curr, result);
            curr.pop();
        }
    }
}

fn probable_fields_for_pos(pos: usize, input: &Input) -> Vec<usize> {
    let mut probables: HashSet<usize> = (0..input.fields.len()).collect();
    // nearby tickets
    let nums: Vec<i64> = input
        .nearby_tickets
        .iter()
        .filter(|&t| is_valid(t, &input.fields))
        .map(|t| t[pos])
        .collect();
    for num in nums {
        probables = probables
            .intersection(&probable_fields_for_num(num, &input.fields))
            .cloned()
            .collect();
    }
    // your ticket
    probables = probables
        .intersection(&probable_fields_for_num(
            input.your_ticket[pos],
            &input.fields,
        ))
        .cloned()
        .collect();
    let mut probables: Vec<usize> = probables.iter().cloned().collect();
    probables.sort();
    probables
}

fn read_test_input() -> Result<Input> {
    std::fs::read_to_string("input/day16_test.in")?.parse()
}

#[test]
fn test_probable_fields_for_pos() {
    let input = read_test_input().unwrap();

    assert_eq!(probable_fields_for_pos(0, &input), vec![0, 1]);
    assert_eq!(probable_fields_for_pos(1, &input), vec![0]);
    assert_eq!(probable_fields_for_pos(2, &input), vec![2]);
}

fn probable_fields_for_num(
    num: i64,
    fields: &Vec<(String, Vec<RangeInclusive<i64>>)>,
) -> HashSet<usize> {
    let mut probable_fields = HashSet::new();
    for (i, (_, ranges)) in fields.iter().enumerate() {
        if ranges.iter().any(|r| r.contains(&num)) {
            probable_fields.insert(i);
        }
    }
    probable_fields
}

#[test]
fn test_probable_fields_for_num() {
    let input = read_test_input().unwrap();

    assert_eq!(
        probable_fields_for_num(7, &input.fields),
        [0, 1].iter().cloned().collect::<HashSet<usize>>()
    );

    assert_eq!(
        probable_fields_for_num(1, &input.fields),
        [0].iter().cloned().collect::<HashSet<usize>>()
    );

    assert_eq!(
        probable_fields_for_num(14, &input.fields),
        [2].iter().cloned().collect::<HashSet<usize>>()
    );
}
