use anyhow::anyhow;
use anyhow::Context;
use anyhow::Result;
use std::{collections::HashMap, str::FromStr};

// --- model

type Address = u64;

#[derive(Debug)]
enum Inst {
    Mask(String),
    Set(Address, u64),
}

impl FromStr for Inst {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("mask = ") {
            // mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
            let relevant = s.strip_prefix("mask = ").unwrap();
            Ok(Inst::Mask(relevant.to_owned()))
        } else if s.starts_with("mem[") {
            // mem[8] = 11
            let mut parts = s.split(' ');
            let address = parts
                .next()
                .with_context(|| format!("invalid mem command"))?
                .split(&['[', ']'][..])
                .nth(1)
                .with_context(|| format!("invalid mem command"))?
                .parse()?;
            let value = parts
                .nth(1)
                .with_context(|| format!("invalid mem command"))?
                .parse()?;
            Ok(Inst::Set(address, value))
        } else {
            Err(anyhow!(format!("invalid instruction string: {}", s)))
        }
    }
}

type Program = Vec<Inst>;

#[derive(Debug)]
struct Machine {
    mask: String,
    mem: HashMap<Address, u64>,
}

impl Machine {
    fn new() -> Self {
        Machine {
            mask: String::new(),
            mem: HashMap::new(),
        }
    }

    fn reset(&mut self) {
        self.mask.clear();
        self.mem.clear();
    }

    fn execute1(&mut self, program: &Program) -> Result<u64> {
        for inst in program {
            match inst {
                Inst::Mask(m) => {
                    self.mask = m.clone();
                }
                Inst::Set(a, v) => {
                    // for every 1 in the mask (at pos i from reverse), "OR" th value by 1 << i
                    // for every 0 in the mask (at pos i from reverse), "AND" th value by ~(1 << i)
                    let mut v = *v;
                    for (i, bit) in self
                        .mask
                        .chars()
                        .rev()
                        .enumerate()
                        .filter(|(_, c)| *c != 'X')
                    {
                        if bit == '0' {
                            v &= !(1 << i);
                        } else if bit == '1' {
                            v |= 1 << i;
                        }
                    }
                    self.mem.insert(*a, v);
                }
            }
        }

        Ok(self.mem.values().sum())
    }

    fn execute2(&mut self, program: &Program) -> Result<u64> {
        // use a simple permutation generation algo.
        // suppose there are 3 unknonws. Then to generate all the permuations of the 3
        // positions, iterate from 0..2^3 and take the binary representation for each number.
        //
        // demonstration:
        // 0 -> 0 0 0
        // 1 -> 0 0 1
        // 2 -> 0 1 0
        // 3 -> 0 1 1
        // 4 -> 1 0 0
        // 5 -> 1 0 1
        // 6 -> 1 1 0
        // 7 -> 1 1 1
        for inst in program {
            match inst {
                Inst::Mask(m) => {
                    self.mask = m.clone();
                }
                Inst::Set(a, v) => {
                    // calculate the result (with 1s, 0s, Xs)
                    let floating = floating_addr(&self.mask, *a);

                    // use these idx to genrate the addresses
                    // store values at each of those addresses
                    let addresses = generate_addresses(&floating);

                    for addr in addresses {
                        self.mem.insert(addr, *v);
                    }
                }
            }
        }

        Ok(self.mem.values().sum())
    }
}

// --- parse input

fn parse_input() -> Result<Program> {
    // std::fs::read_to_string("input/day14_test2.in")
    // std::fs::read_to_string("input/day14_test.in")
    std::fs::read_to_string("input/day14.in")
        .with_context(|| String::from("failed to read input file"))?
        .lines()
        .map(|line| line.parse::<Inst>())
        .collect()
}

// --- utils

fn generate_addresses(floating: &str) -> Vec<Address> {
    // identify count and idx for all Xs
    let x_idx = x_idx(&floating);

    // replace all X with 0 and construct a base address
    let base = base_addr(&floating);

    let mut addresses = vec![];

    for n in 0..2u64.pow(x_idx.len() as u32) {
        let mut addr = base;
        for i in 0..x_idx.len() {
            // i'th bit of n
            let ni = (n >> i) & 1;
            if ni == 0 {
                set0(&mut addr, x_idx[i]);
            } else {
                set1(&mut addr, x_idx[i]);
            }
        }
        addresses.push(addr);
    }

    addresses
}

#[test]
fn test_generate_addresses() {
    // 000000000000000000000000000000X1101X
    // 000000000000000000000000000000011010  (decimal 26)
    // 000000000000000000000000000000011011  (decimal 27)
    // 000000000000000000000000000000111010  (decimal 58)
    // 000000000000000000000000000000111011  (decimal 59)

    let floating = "000000000000000000000000000000X1101X";
    assert_eq!(generate_addresses(&floating), vec![26, 27, 58, 59]);
}

// find the base address by replacing all 'x' with '0' and converting to u64
fn base_addr(floating: &str) -> u64 {
    let floating = floating.replace("X", "0");
    u64::from_str_radix(&floating, 2).unwrap()
}

#[test]
fn test_base_addr() {
    // result:  000000000000000000000000000000X1101X
    let floating = floating_addr("000000000000000000000000000000X1001X", 42);
    assert_eq!(base_addr(&floating), 26);
}

// find the idx of 'X' in the floating addr
fn x_idx(floating: &str) -> Vec<usize> {
    floating
        .chars()
        .rev()
        .enumerate()
        .filter(|(_, ch)| *ch == 'X')
        .map(|(i, _)| i)
        .collect()
}

#[test]
fn test_x_idx() {
    // result:  000000000000000000000000000000X1101X
    assert_eq!(x_idx("000000000000000000000000000000X1101X"), vec![0, 5]);
}

// calculate the floating address
fn floating_addr(mask: &str, address: u64) -> String {
    // 0 => unchanged (& with 1)
    // 1 => overwritten with 1 (| with 0)
    // X => floating bit
    let binary_address = format!("{:0width$b}", address, width = 36);
    mask.chars()
        .zip(binary_address.chars())
        .map(|(mi, ai)| match mi {
            '0' => ai,
            '1' => '1',
            'X' => 'X',
            _ => unreachable!(),
        })
        .collect()
}

#[test]
fn test_floating_addr() {
    // address: 000000000000000000000000000000101010  (decimal 42)
    // mask:    000000000000000000000000000000X1001X
    // result:  000000000000000000000000000000X1101X
    assert_eq!(
        floating_addr("000000000000000000000000000000X1001X", 42),
        "000000000000000000000000000000X1101X".to_owned()
    );
}

// set the `idx` bit to 0
fn set0(number: &mut u64, idx: usize) {
    *number &= !(1 << idx);
}

#[test]
fn test_set0() {
    // 000000000000000000000000000000011010  (decimal 26)
    // 000000000000000000000000000000011011  (decimal 27)
    // 000000000000000000000000000000111010  (decimal 58)
    // 000000000000000000000000000000111011  (decimal 59)
    let mut x = 59;

    set0(&mut x, 0);
    assert_eq!(x, 58);

    set0(&mut x, 5);
    assert_eq!(x, 26);
}

// set the `idx` bit to 1
fn set1(number: &mut u64, idx: usize) {
    *number |= 1 << idx;
}

#[test]
fn test_set1() {
    // 000000000000000000000000000000011010  (decimal 26)
    // 000000000000000000000000000000011011  (decimal 27)
    // 000000000000000000000000000000111010  (decimal 58)
    // 000000000000000000000000000000111011  (decimal 59)
    let mut x = 26;

    set1(&mut x, 0);
    assert_eq!(x, 27);

    set1(&mut x, 5);
    assert_eq!(x, 59);
}

// --- main

fn main() -> Result<()> {
    let input = parse_input()?;

    let mut m = Machine::new();

    println!("part1: {}", part1(&mut m, &input)?);
    println!("part2: {}", part2(&mut m, &input)?);

    Ok(())
}

fn part1(m: &mut Machine, input: &Program) -> Result<u64> {
    m.reset();
    m.execute1(input)
}

fn part2(m: &mut Machine, input: &Program) -> Result<u64> {
    m.reset();
    m.execute2(input)
}
