use anyhow::Context;
use anyhow::Result;

// --- model

type Timestamp = i64;
type BusID = i64;

struct Input {
    timestamp: Timestamp,
    bus_ids: Vec<Option<BusID>>,
}

// --- parse input

fn parse_input() -> Result<Input> {
    // let input = std::fs::read_to_string("input/day13_test.in")
    let input = std::fs::read_to_string("input/day13.in")
        .with_context(|| format!("error reading input file"))?;

    let mut lines = input.lines();
    let timestamp = lines.next().unwrap().parse()?;
    let bus_ids = lines
        .next()
        .unwrap()
        .split(',')
        .map(|s| s.parse::<i64>().ok())
        .collect();

    Ok(Input { timestamp, bus_ids })
}

// --- solution

fn main() -> Result<()> {
    let input = parse_input()?;

    println!("part1: {}", part1(&input)?);
    println!("part2: {}", part2(&input)?);

    Ok(())
}

fn part1(input: &Input) -> Result<i64> {
    let partial = input
        .bus_ids
        .iter()
        .filter_map(|bid| *bid)
        .map(|bno| (bno, input.timestamp % bno))
        .map(|(bno, remaining)| {
            if remaining == 0 {
                (bno, 0)
            } else {
                (bno, bno - remaining)
            }
        })
        .min_by_key(|x| x.1)
        .expect("unable to find a minimum");

    Ok(partial.0 * partial.1)
}

fn part2(input: &Input) -> Result<Timestamp> {
    // logic:
    // once we find a reasonable timestamp for busses b1 and b2, such timestamps
    // will keep repeating every LCM(b1, b2) intervals

    let mut ts: i64 = 0;
    let mut step: i64 = 1;

    for (offset, bus_id) in input.bus_ids.iter().enumerate() {
        if let Some(bus_id) = bus_id {
            loop {
                if (ts + offset as i64) % (bus_id) == 0 {
                    // ts works for this bus
                    // update the multiplier and move to the next bus
                    step *= bus_id;
                    break;
                } else {
                    // try the next possible timestamp
                    ts += step;
                }
            }
        }
    }

    Ok(ts)
}
