fn main() -> Result<(), Box<dyn std::error::Error>> {
    // let input: Vec<u64> = std::fs::read_to_string("input/day9_test.in")?
    let input: Vec<u64> = std::fs::read_to_string("input/day9.in")?
        .lines()
        .map(str::parse::<u64>)
        .collect::<Result<Vec<_>, _>>()?;

    let preamble_len = 25;

    let ans1 = (preamble_len..input.len())
        .find_map(|idx| {
            if invalid(&input, idx, preamble_len) {
                Some(input[idx])
            } else {
                None
            }
        })
        .expect("no answer found for part 1");
    println!("part1: {}", ans1);

    // check values from idx-preamble_len..idx and find the combination
    let ans2 = find_seq(&input, ans1).expect("no answer found for part 2");
    println!("part2: {}", ans2);
    Ok(())
}

fn find_seq(data: &Vec<u64>, target: u64) -> Option<u64> {
    for i in 0..data.len() - 1 {
        for j in i + 1..data.len() {
            let range = &data[i..=j];
            if range.iter().sum::<u64>() == target {
                return Some(range.iter().min().unwrap() + range.iter().max().unwrap());
            }
        }
    }
    None
}

fn invalid(data: &Vec<u64>, idx: usize, preamble_len: usize) -> bool {
    let start = idx - preamble_len;
    let end = idx;
    for i in start..end - 1 {
        for j in start + 1..end {
            if data[i] + data[j] == data[idx] {
                return false;
            }
        }
    }
    return true;
}
