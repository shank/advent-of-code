use std::collections::HashMap;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // let mut input: Vec<usize> = std::fs::read_to_string("input/day10_test.in")?
    // let input: Vec<usize> = std::fs::read_to_string("input/day10_test2.in")?
    let mut input: Vec<usize> = std::fs::read_to_string("input/day10.in")?
        .lines()
        .map(|s| s.parse::<usize>().unwrap())
        .collect();

    prepare_input(&mut input);

    let diffs = calc_adapter_diffs(&input);
    let ans1 = diffs[&1] * diffs[&3];
    println!("part1: {}", ans1);

    let ways = calc_adapter_ways(&input);
    let ans2 = ways[ways.len() - 1];
    println!("part2: {}", ans2);

    Ok(())
}

fn calc_adapter_ways(ratings: &[usize]) -> Vec<usize> {
    let mut result = vec![0; ratings.len()];
    result[0] = 1;

    for i in 1..ratings.len() {
        let max_prev = std::cmp::max(0, i as i32 - 3) as usize;
        let mut ways = 0;
        for p in max_prev..i {
            if ratings[i] - ratings[p] <= 3 {
                ways += result[p];
            }
        }
        result[i] = ways;
    }

    result
}

fn prepare_input(ratings: &mut Vec<usize>) {
    ratings.push(0);
    ratings.push(*ratings.iter().max().unwrap() + 3);
    ratings.sort();
}

#[test]
fn test_calc_adapter_ways() {
    let mut ratings = vec![16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4];
    prepare_input(&mut ratings);

    let ans = calc_adapter_ways(&ratings);
    dbg!(&ans);
    assert_eq!(ans[ratings.len() - 1], 8);
}

#[test]
fn test_calc_adapter_diffs() {
    let mut ratings = vec![16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4];
    prepare_input(&mut ratings);

    let ans = calc_adapter_diffs(&ratings);
    dbg!(&ans);
    assert_eq!(ans.get(&1), Some(&7));
    assert_eq!(ans.get(&3), Some(&5));
}

fn calc_adapter_diffs(ratings: &[usize]) -> HashMap<usize, usize> {
    let mut result: HashMap<usize, usize> = HashMap::new();

    let diffs: Vec<usize> = ratings
        .iter()
        .zip(&ratings[1..])
        .map(|(r1, r2)| r2 - r1)
        .collect();

    for d in diffs {
        let entry = result.entry(d).or_default();
        *entry += 1;
    }

    result
}
