defmodule Aoc2018.Day13 do
  # ================================
  # REMEMBER
  # ---------------------
  # Make it Work
  # Make it Beautiful
  # Make it Fast
  # ================================

  def main() do
    parse_input() |> part1() |> IO.inspect()
    parse_input() |> part2() |> IO.inspect()
  end

  def part1(state) do
    simulate(state)
  end

  def part2(state) do
    Enum.reduce_while(Stream.cycle([true]), state, fn _, state ->
      nil
    end)
  end

  def simulate(state) do
    Enum.reduce_while(Stream.cycle([true]), state, fn _, state ->
      case step(state) do
        {:collision, position} -> {:halt, position}
        updated_state -> {:cont, updated_state}
      end
    end)
  end

  def step(state, updated_carts \\ [])

  def step(%{carts: []} = state, updated_carts), do: %{state | carts: sort_carts(updated_carts)}

  def step(
        %{
          tracks: tracks,
          carts: [cart | rest],
          carts_positions: carts_positions,
          collisions: collisions
        } = state,
        updated_carts
      ) do
    updated_cart = update_cart(tracks, cart)

    carts_positions =
      carts_positions |> MapSet.delete(cart.position) |> MapSet.put(updated_cart.position)

    IO.inspect(updated_cart, label: "updated_cart")
    # IO.gets("")

    if MapSet.member?(carts_positions, updated_cart.position) do
      # remove the collided carts
      other_idx = Enum.find_index(rest, fn c -> c.position == updated_cart.position end)
      {_, rest} = List.pop_at(rest, other_idx)

      carts_positions = MapSet.delete(carts_positions, updated_cart.position)

      # record collision position
      collisions = [updated_cart.position | collisions]

      step(
        %{state | carts: rest, carts_positions: carts_positions},
        updated_carts
      )
    else
      step(
        %{state | carts: rest, carts_positions: carts_positions},
        [updated_cart | updated_carts]
      )
    end
  end

  def update_cart(tracks, cart) do
    moved_cart = move_cart(cart)

    updated_cart =
      case Map.get(tracks, moved_cart.position) do
        ?\\ -> update_at_turn(moved_cart, ?\\)
        ?/ -> update_at_turn(moved_cart, ?/)
        ?+ -> update_at_intersection(moved_cart)
        _ -> moved_cart
      end

    updated_cart
  end

  def update_at_turn(cart, turn)

  def update_at_turn(%{direction: direction} = cart, ?\\) do
    new_direction =
      case direction do
        :up -> :left
        :right -> :down
        :down -> :right
        :left -> :up
      end

    %{cart | direction: new_direction}
  end

  def update_at_turn(%{direction: direction} = cart, ?/) do
    new_direction =
      case direction do
        :up -> :right
        :right -> :up
        :down -> :left
        :left -> :down
      end

    %{cart | direction: new_direction}
  end

  def new_direction(direction, turn) do
    case direction do
      :up ->
        case turn do
          :left -> :left
          :straight -> :up
          :right -> :right
        end

      :right ->
        case turn do
          :left -> :up
          :straight -> :right
          :right -> :down
        end

      :down ->
        case turn do
          :left -> :right
          :straight -> :down
          :right -> :left
        end

      :left ->
        case turn do
          :left -> :down
          :straight -> :left
          :right -> :up
        end
    end
  end

  def update_direction_at_intersection(%{direction: direction, next_turn: next_turn} = cart) do
    %{cart | direction: new_direction(direction, next_turn)}
  end

  def update_next_turn(cart) do
    %{cart | next_turn: next_turn(cart)}
  end

  def update_at_intersection(cart) do
    cart
    |> update_direction_at_intersection
    |> update_next_turn()
  end

  def move_cart(cart), do: %{cart | position: new_position(cart)}

  def new_position(%{position: {x, y}, direction: direction} = _cart) do
    case direction do
      :right -> {x + 1, y}
      :left -> {x - 1, y}
      :up -> {x, y - 1}
      :down -> {x, y + 1}
    end
  end

  def read_input() do
    File.read!("input/day_13.txt")
    |> String.split("\n")
  end

  def parse_input() do
    empty_state = %{
      tracks: %{},
      carts: [],
      carts_positions: MapSet.new(),
      collisions: []
    }

    state =
      read_input()
      |> Stream.with_index()
      |> Enum.reduce(empty_state, fn {line, r}, state ->
        line
        |> to_charlist()
        |> Stream.with_index()
        |> Enum.reduce(state, fn {ch, c},
                                 %{tracks: tracks, carts: carts, carts_positions: carts_positions} =
                                   state ->
          {updated_tracks, updated_carts, updated_carts_positions} =
            case ch do
              ?> ->
                {Map.put(tracks, {c, r}, ?-),
                 [
                   %{_id: length(carts), position: {c, r}, direction: :right, next_turn: :left}
                   | carts
                 ], MapSet.put(carts_positions, {c, r})}

              ?v ->
                {Map.put(tracks, {c, r}, ?|),
                 [
                   %{_id: length(carts), position: {c, r}, direction: :down, next_turn: :left}
                   | carts
                 ], MapSet.put(carts_positions, {c, r})}

              ?< ->
                {Map.put(tracks, {c, r}, ?-),
                 [
                   %{_id: length(carts), position: {c, r}, direction: :left, next_turn: :left}
                   | carts
                 ], MapSet.put(carts_positions, {c, r})}

              ?^ ->
                {Map.put(tracks, {c, r}, ?|),
                 [
                   %{_id: length(carts), position: {c, r}, direction: :up, next_turn: :left}
                   | carts
                 ], MapSet.put(carts_positions, {c, r})}

              _ ->
                {Map.put(tracks, {c, r}, ch), carts, carts_positions}
            end

          %{
            state
            | tracks: updated_tracks,
              carts: updated_carts,
              carts_positions: updated_carts_positions
          }
        end)
      end)

    %{state | carts: sort_carts(state.carts)}
  end

  def next_turn(cart) do
    case cart.next_turn do
      :left -> :straight
      :straight -> :right
      :right -> :left
    end
  end

  def sort_carts(carts) do
    Enum.sort_by(carts, fn cart ->
      {x, y} = cart.position
      {y, x}
    end)
  end
end
