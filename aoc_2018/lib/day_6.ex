defmodule Aoc2018.Day6 do
  @type coordinate :: %{x: integer, y: integer, id: integer}
  @type closest :: %{%{x: integer, y: integer} => %{id: integer, distance: integer}}

  @spec input_lines() :: [String.t()]
  defp input_lines() do
    File.read!("input/day_6.txt")
    |> String.split("\n", trim: true)
  end

  @spec parse_coordinate(unparsed_line :: String.t()) :: %{x: integer, y: integer}
  def parse_coordinate(line) do
    [x, y] = line |> String.split(", ") |> Enum.map(&String.to_integer/1)
    %{x: x, y: y}
  end

  @spec main() :: :ok
  def main() do
    coordinates =
      input_lines()
      |> Enum.map(&parse_coordinate/1)
      |> Enum.with_index()
      |> Enum.map(fn {coordinate, index} -> Map.put(coordinate, :id, index + 1) end)

    part1(coordinates) |> IO.puts()
    part2(coordinates) |> IO.puts()
  end

  @spec part1([coordinate]) :: nil
  def part1(coordinates) do
    bounds = bounding_box(coordinates)

    closest_map =
      bounds
      |> all_locations()
      |> Stream.map(&{&1, closest(&1, coordinates)})
      |> Map.new()

    infinite_ids = infinite_ids(bounds, closest_map)

    closest_map
    |> Map.values()
    # [%{id: owner, distance: distance}]
    |> Enum.reject(&(&1.id == :tie or MapSet.member?(infinite_ids, &1.id)))
    # [%{id: owner, distance: distance}]
    |> Enum.group_by(& &1.id)
    # %{ (id :: integer) => [owner_and_distance]}
    |> Map.values()
    |> Enum.map(&length(&1))
    |> Enum.max()
  end

  @spec part2([coordinate]) :: non_neg_integer()
  def part2(coordinates) do
    coordinates
    |> bounding_box()
    |> all_locations()
    |> Stream.map(&total_distance(&1, coordinates))
    |> Stream.filter(&(&1 < 10_000))
    |> Enum.count()
  end

  @spec total_distance(%{x: integer, y: integer}, [coordinate]) :: integer
  defp total_distance(location, coordinates) do
    coordinates
    |> Stream.map(&manhattan_distance(location, &1))
    |> Enum.sum()
  end

  @spec closest(coordinate, [coordinate]) :: coordinate
  defp closest(location, coordinates) do
    Enum.reduce(coordinates, nil, fn coord, current_closest ->
      new_distance = manhattan_distance(location, coord)

      cond do
        is_nil(current_closest) -> %{id: coord.id, distance: new_distance}
        new_distance > current_closest.distance -> current_closest
        new_distance < current_closest.distance -> %{id: coord.id, distance: new_distance}
        new_distance == current_closest.distance -> %{current_closest | id: :tie}
      end
    end)
  end

  @spec infinite_ids(bounds :: {Range.t(), Range.t()}, closest) :: MapSet.t(integer)
  defp infinite_ids(bounds, closest_map) do
    # from the owners of all_locations, get a list of owners who own a location on the bounding box
    {left..right, top..bottom} = bounds

    closest_map
    |> Stream.filter(fn {%{x: x, y: y}, _owner_and_distance} ->
      x in [left, right] or y in [top, bottom]
    end)
    |> Stream.map(fn {_location, %{id: owner}} -> owner end)
    |> MapSet.new()
  end

  @spec manhattan_distance(coordinate, coordinate) :: integer
  defp manhattan_distance(coordinate1, coordinate2) do
    abs(coordinate1.x - coordinate2.x) + abs(coordinate1.y - coordinate2.y)
  end

  @spec bounding_box([coordinate]) :: {x_range :: Range.t(), y_range :: Range.t()}
  def bounding_box(coordinates) do
    {%{x: x_min}, %{x: x_max}} = Enum.min_max_by(coordinates, & &1.x)
    {%{y: y_min}, %{y: y_max}} = Enum.min_max_by(coordinates, & &1.y)
    {x_min..x_max, y_min..y_max}
  end

  @spec all_locations({Range.t(), Range.t()}) :: [%{x: integer, y: integer}]
  def all_locations({x_range, y_range}) do
    for x <- x_range,
        y <- y_range,
        do: %{x: x, y: y}
  end
end
