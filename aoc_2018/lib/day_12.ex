defmodule Aoc2018.Day12 do
  @rules_lines """
  .##.# => #
  ##.#. => #
  ##... => #
  #.... => .
  .#..# => .
  #.##. => .
  .##.. => .
  .#.## => .
  ###.. => .
  ..##. => #
  ##### => #
  #...# => #
  .#... => #
  ###.# => #
  #.### => #
  ##..# => .
  .###. => #
  ...## => .
  ..#.# => .
  ##.## => #
  ....# => .
  #.#.# => #
  #.#.. => .
  .#### => .
  ...#. => #
  ..### => .
  ..#.. => #
  ..... => .
  ####. => .
  #..## => #
  .#.#. => .
  #..#. => #
  """
  def rules_lines(), do: @rules_lines |> String.split("\n", trim: true)

  # =========================
  # REMEMBER
  # --------
  # Make it Work
  # Make it Beautiful
  # Make it Fast
  # =========================

  def main() do
    {plants, rules} = parse_input()

    part1(plants, rules) |> IO.puts()
    part2(plants, rules) |> IO.puts()
  end

  def part1(plants, rules), do: plants |> simulate(rules, 20) |> score
  def part2(plants, rules), do: plants |> simulate(rules, 50_000_000_000) |> score

  def score(plants), do: plants |> Enum.sum()

  def simulate(plants, _rules, 0), do: plants

  def simulate(plants, rules, num_steps) do
    next_plants = transform_plants(plants, rules)

    case shifted?(plants, next_plants) do
      {:yes, shift} ->
        plants |> Enum.map(&(&1 + shift * num_steps)) |> MapSet.new()

      :no ->
        simulate(next_plants, rules, num_steps - 1)
    end
  end

  def shifted?(old_plants, new_plants) do
    old = old_plants |> MapSet.to_list() |> Enum.sort()
    new = new_plants |> MapSet.to_list() |> Enum.sort()

    diffs =
      new
      |> Stream.zip(old)
      |> Stream.map(fn {v1, v2} -> v1 - v2 end)
      |> Enum.dedup()

    if length(diffs) == 1 do
      {:yes, hd(diffs)}
    else
      :no
    end
  end

  def transform_plants(plants, rules) do
    {min, max} = Enum.min_max(plants)

    Enum.reduce((min - 3)..(max + 3), MapSet.new(), fn mid_idx, acc ->
      if next_state(plants, rules, mid_idx),
        do: MapSet.put(acc, mid_idx),
        else: MapSet.delete(acc, mid_idx)
    end)
  end

  def next_state(plants, rules, mid_idx) do
    Enum.reduce_while(rules, nil, fn {pattern, pot}, _acc ->
      if pattern_match?(plants, mid_idx, pattern) do
        {:halt, pot}
      else
        {:cont, nil}
      end
    end)
  end

  def pattern_match?(plants, mid_idx, pattern) do
    pattern
    |> Enum.with_index(-2)
    |> Enum.all?(fn {should_exist, offset} ->
      (should_exist == true && MapSet.member?(plants, mid_idx + offset)) or
        (should_exist == false && not MapSet.member?(plants, mid_idx + offset))
    end)
  end

  def parse_input() do
    [h | rest] =
      File.read!("input/day_12.txt")
      |> String.trim()
      |> String.split("\n", trim: true)

    {parse_plants(h), parse_rules(rest)}
  end

  def parse_plants(<<"initial state: ", plants::binary>>) do
    plants
    |> to_charlist
    |> Stream.with_index()
    |> Stream.filter(&match?({?#, _index}, &1))
    |> Stream.map(fn {_pot, index} -> index end)
    |> MapSet.new()
  end

  def parse_rules(lines) do
    lines
    |> Stream.map(&parse_rule_line/1)
    |> Map.new()
  end

  def parse_rule_line(<<pattern::binary-size(5), " => ", pot::binary-size(1)>>) do
    {parse_pattern(pattern), parse_pot(pot)}
  end

  def parse_pattern(pattern) do
    pattern
    |> to_charlist()
    |> Enum.map(&((match?(?#, &1) && true) || false))
  end

  def parse_pot(pot), do: if(pot == "#", do: true, else: false)
end
