defmodule Aoc2018.Day4 do
  @moduledoc """
  Documentation for Day4.
  """

  import NimbleParsec

  @type unparsed_record :: String.t()

  @type parsec_result ::
          {:ok, list(), String.t(), map(), integer, integer}
          | {:error, String.t(), String.t(), map(), integer, integer}

  @typedoc """
  {{year, month, day}, hour, minute, {guard_id} | :sleeps | :wakes_up}
  """
  @type record ::
          {{non_neg_integer, non_neg_integer, non_neg_integer}, non_neg_integer, non_neg_integer,
           {non_neg_integer} | :sleeps | :wakes_up}

  @type guard_data :: %{non_neg_integer => [Range.t()]}

  @spec input() :: [String.t()]
  def input() do
    File.read!("input/day_4.txt")
    |> String.trim()
    |> String.split("\n")
  end

  @spec main() :: :ok
  def main() do
    input()
    |> parse_records()
    |> compile_guard_data()
    |> part1()
    |> IO.puts()

    input()
    |> parse_records()
    |> compile_guard_data()
    |> part2()
    |> IO.puts()
  end

  @spec parse_records([unparsed_record]) :: [record]
  def parse_records(unparsed_records) do
    unparsed_records
    |> Enum.map(&parse_record_line/1)
    |> Enum.map(&to_record/1)
  end

  # [1518-11-01 00:00] Guard #10 begins shift
  # [1518-11-01 00:05] falls asleep
  # [1518-11-01 00:25] wakes up

  datetime =
    ignore(string("["))
    |> integer(4)
    |> ignore(string("-"))
    |> integer(2)
    |> ignore(string("-"))
    |> integer(2)
    |> ignore(string(" "))
    |> integer(2)
    |> ignore(string(":"))
    |> integer(2)
    |> ignore(string("] "))

  action =
    choice([
      string("falls asleep"),
      string("wakes up"),
      ignore(string("Guard #"))
      |> integer(min: 1)
      |> ignore(string(" begins shift"))
    ])

  defparsec(:parse_record_line, datetime |> concat(action))

  @doc """
  parsec_result -> record.

  ## Examples

      iex> Day4.parse_record_line("[1518-11-01 00:00] Guard #10 begins shift") |> Day4.to_record
      {[1518, 11, 1], 0, 0, 10}

      iex> Day4.parse_record_line("[1518-11-01 00:05] falls asleep") |> Day4.to_record
      {[1518, 11, 1], 0, 5, :sleeps}

      iex> Day4.parse_record_line("[1518-11-01 00:25] wakes up") |> Day4.to_record
      {[1518, 11, 1], 0, 25, :wakes_up}

  """
  @spec to_record(parsec_result) :: record
  def to_record({:ok, [year, month, date, hour, minute, action], _, _, _, _}) do
    case action do
      "wakes up" -> {[year, month, date], hour, minute, :wakes_up}
      "falls asleep" -> {[year, month, date], hour, minute, :sleeps}
      id -> {[year, month, date], hour, minute, id}
    end
  end

  @spec compile_guard_data([record]) :: guard_data
  def compile_guard_data(records) do
    records
    # sort the records by date and time
    |> Enum.sort()
    # construct the id => [start..end, start2..end2, ...] structure for each day
    |> Enum.reduce({{nil, nil}, %{}}, fn record, {{guard_id, sleep_start}, acc} ->
      case record do
        {_, _, minute, :wakes_up} ->
          {{guard_id, sleep_start},
           Map.update(
             acc,
             guard_id,
             [sleep_start..(minute - 1)],
             &[sleep_start..(minute - 1) | &1]
           )}

        {_, _, minute, :sleeps} ->
          {{guard_id, minute}, acc}

        {_, _, _minute, id} ->
          {{id, nil}, acc}
      end
    end)
    |> elem(1)
  end

  @spec part1(guard_data) :: non_neg_integer
  def part1(guard_data) do
    # make it work [x]
    # make it beautiful [x] -> workable...
    # make it fast []

    # find the guard who slept the most
    slept_most =
      guard_data
      |> Enum.map(fn {guard_id, sleep_times} ->
        total = sleep_times |> Enum.map(fn s..e -> e - s + 1 end) |> Enum.sum()
        {guard_id, total}
      end)
      |> Enum.max_by(fn {_guard_id, total} -> total end)
      |> elem(0)

    # get the record for this guard and find the minute which overlaps in most range... maybe iterate from 0 to 59?
    chosen_minute =
      guard_data
      |> Map.get(slept_most)
      |> Enum.reduce(%{}, fn s..e, acc ->
        Enum.reduce(s..e, acc, fn minute, acc ->
          Map.update(acc, minute, 1, &(&1 + 1))
        end)
      end)
      |> Enum.max_by(fn {_m, t} -> t end)
      |> elem(0)

    chosen_minute * slept_most
  end

  @spec part2(guard_data) :: non_neg_integer
  def part2(guard_data) do
    {guard_id, max_min, _max_f} =
      guard_data
      |> Enum.map(fn {guard_id, sleep_times} ->
        sleep_minute_freq =
          Enum.reduce(sleep_times, %{}, fn s..e, acc ->
            Enum.reduce(s..e, acc, fn m, acc ->
              Map.update(acc, m, 1, &(&1 + 1))
            end)
          end)

        {guard_id, sleep_minute_freq}
      end)
      |> Enum.map(fn {guard_id, sleep_minute_freq} ->
        {max_min, max_f} = Enum.max_by(sleep_minute_freq, fn {_m, f} -> f end)
        {guard_id, max_min, max_f}
      end)
      |> Enum.max_by(fn {_guard_id, _max_min, max_f} -> max_f end)

    guard_id * max_min
  end
end
