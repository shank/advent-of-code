defmodule Aoc2018.Day9 do
  defmodule Game do
    @enforce_keys [:players, :last_marble, :circle, :curr_player, :curr_marble, :scores]
    defstruct [:players, :last_marble, :circle, :curr_player, :curr_marble, :scores]
  end

  alias ElixirDS.Deque

  def main() do
    parse_input()
    |> simulate_game()
    |> (fn game -> game.scores end).()
    |> Enum.max_by(&elem(&1, 1))
    |> elem(1)
    |> IO.puts()
  end

  defp parse_input() do
    [players, last_marble_worth] =
      File.read!("input/day_9.txt")
      |> String.trim()
      |> String.split([" players; last marble is worth ", " points"], trim: true)
      |> Enum.map(&String.to_integer/1)

    {players, last_marble_worth}
  end

  def simulate_game({players, last_marble}) do
    game = %Game{
      players: players,
      last_marble: last_marble,
      circle: Deque.from_list([0]),
      curr_player: -1,
      curr_marble: 0,
      scores: %{}
    }

    do_game(game)
  end

  def do_game(%Game{curr_marble: curr_marble, last_marble: last_marble} = game)
      when curr_marble == last_marble,
      do: game

  def do_game(game) do
    # IO.inspect(game, label: "game")
    # IO.gets("continue?")

    game = insert_marble(game)

    do_game(game)
  end

  def insert_marble(
        game = %Game{
          players: players,
          last_marble: _last_marble,
          circle: circle,
          curr_player: curr_player,
          curr_marble: curr_marble,
          scores: scores
        }
      )
      when rem(curr_marble + 1, 23) == 0 do
    marble = curr_marble + 1
    player = rem(curr_player + 1, players)

    circle = Deque.rotate(circle, 7)
    {val, circle} = Deque.pop_back(circle)
    circle = Deque.rotate(circle, -1)

    scores =
      scores
      |> Map.update(player, marble + val, &(&1 + marble + val))

    %Game{
      game
      | circle: circle,
        curr_marble: marble,
        curr_player: player,
        scores: scores
    }
  end

  def insert_marble(
        game = %Game{
          players: players,
          circle: circle,
          curr_player: curr_player,
          curr_marble: curr_marble,
          scores: _scores
        }
      ) do
    marble = curr_marble + 1
    player = rem(curr_player + 1, players)

    circle =
      circle
      |> Deque.rotate(-1)
      |> Deque.push_back(marble)

    %Game{game | circle: circle, curr_marble: marble, curr_player: player}
  end
end
