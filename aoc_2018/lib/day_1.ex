defmodule Aoc2018.Day1 do
  def read_input() do
    File.read!("input/day_1.txt")
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&String.to_integer/1)
  end

  def main() do
    read_input()
    |> part1()
    |> IO.puts()

    read_input()
    |> part2()
    |> IO.puts()
  end

  def part1(changes) do
    changes
    |> Enum.sum()
  end

  def part2(changes) do
    changes
    |> Stream.cycle()
    |> Enum.reduce_while({0, MapSet.new()}, fn change, {prev, freqs} ->
      next = prev + change

      if MapSet.member?(freqs, next) do
        {:halt, next}
      else
        {:cont, {next, MapSet.put(freqs, next)}}
      end
    end)
  end
end
