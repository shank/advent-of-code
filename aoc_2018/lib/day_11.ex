defmodule Aoc2018.Day11 do
  @grid_serial_number 4455
  @grid_size 300

  @type coordinate :: {non_neg_integer, non_neg_integer}
  @type power :: integer
  @type coordinate_power :: %{coordinate => power}

  def main() do
    coordinates = all_coordinates(@grid_size)
    powers = all_powers(coordinates)
    sat_powers = sat_powers(@grid_size, powers)

    part1_result = part1(sat_powers)
    IO.puts("part1: #{inspect(part1_result)}")

    part2_result = part2(sat_powers)
    IO.puts("part2: #{inspect(part2_result)}")
  end

  @spec part2(coordinate_power) :: {coordinate, {pos_integer, power}}
  defp part2(sat_powers) do
    %{x: x, y: y, size: size, power: power} =
      Enum.reduce(1..@grid_size, nil, fn x, best ->
        # IO.puts("processing for x: #{x}")

        Enum.reduce(1..@grid_size, best, fn y, best ->
          # for {x, y} the squares will be from size 1 to 300 - max(x, y)
          m = 300 - max(x, y) + 1

          Enum.reduce(1..m, best, fn size, best ->
            value = square_power({x, y}, size, sat_powers)

            if best == nil or value > best.power do
              %{x: x, y: y, size: size, power: value}
            else
              best
            end
          end)
        end)
      end)

    {{x, y}, {size, power}}
  end

  @spec part1(coordinate_power) :: coordinate
  defp part1(sat_powers) do
    %{x: x, y: y, power: _power} =
      Enum.reduce(1..(@grid_size - 2), nil, fn x, best ->
        Enum.reduce(1..(@grid_size - 2), best, fn y, best ->
          value = square_power({x, y}, 3, sat_powers)

          if best == nil or value > best.power do
            %{x: x, y: y, power: value}
          else
            best
          end
        end)
      end)

    {x, y}
  end

  # power for a particular coordinate.
  @doc """
      iex> Day11.power({33, 45})
      4
      iex> Day11.power({35, 47})
      4
  """
  @spec power(coordinate) :: power
  def power({x, y}) do
    rack_id = x + 10

    power =
      ((rack_id * y + @grid_serial_number) * rack_id)
      |> rem(1000)
      |> div(100)

    power - 5
  end

  # all coordinates for the grid
  @spec all_coordinates(pos_integer) :: [coordinate]
  defp all_coordinates(size) do
    for x <- 1..size, y <- 1..size, do: {x, y}
  end

  # powers for each coordinate of the grid
  @spec all_powers([coordinate]) :: %{coordinate => power}
  defp all_powers(coordinates) do
    Enum.reduce(coordinates, %{}, &Map.put(&2, &1, power(&1)))
  end

  # Summed Area Table values for the grid coordinates.
  @spec sat_powers(pos_integer, coordinate_power) :: coordinate_power
  defp sat_powers(size, powers) do
    Enum.reduce(1..size, %{}, fn x, acc ->
      Enum.reduce(1..size, acc, fn y, acc ->
        top = Map.get(acc, {x, y - 1}, 0)
        left = Map.get(acc, {x - 1, y}, 0)
        top_left = Map.get(acc, {x - 1, y - 1}, 0)
        curr = Map.get(powers, {x, y})
        Map.put(acc, {x, y}, top + left - top_left + curr)
      end)
    end)
  end

  # power for a square starting at `{x, y}` and having width of `size`
  @spec square_power(coordinate, pos_integer, coordinate_power) :: pos_integer
  defp square_power({x, y}, size, sat_powers) do
    bottom_right = Map.get(sat_powers, {x + size - 1, y + size - 1})
    top_left_minus_one = Map.get(sat_powers, {x - 1, y - 1}, 0)
    top = Map.get(sat_powers, {x + size - 1, y - 1}, 0)
    left = Map.get(sat_powers, {x - 1, y + size - 1}, 0)

    bottom_right + top_left_minus_one - top - left
  end
end
