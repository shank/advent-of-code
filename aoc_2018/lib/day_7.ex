defmodule Aoc2018.Day7 do
  require Logger

  # :idle | {:busy, "A"} | {:done, "A"}
  @type status :: atom | {atom, String.t()}

  def main() do
    dependency_map = generate_dependency_map()

    part1(dependency_map)
    part2(dependency_map, n_workers: 5, base_time: 60)
  end

  defp part1(dependency_map) do
    find_order(dependency_map, [])
    |> Enum.join()
    |> IO.inspect()
  end

  defp part2(dependency_map, opts) do
    n_workers = Keyword.get(opts, :n_workers, 1)
    base_time = Keyword.get(opts, :base_time, 0)

    workers = create_workers(n_workers)
    # execute the tasks using the workers, and keep a track of the time
    time_taken = execute(dependency_map, workers, base_time)
    IO.puts(time_taken)
  end

  defp assign_tasks([task | tasks], [pid | pids], base_time) do
    ch = task |> to_charlist() |> hd
    task_time = ch - ?A + 1 + base_time
    send(pid, {:schedule_task, {task, task_time}})
    assign_tasks(tasks, pids, base_time)
  end

  defp assign_tasks(_, _, _) do
  end

  defp find_idle_workers(workers) do
    workers
    |> Enum.filter(fn {_worker, state} -> state == :idle end)
    |> Enum.map(&elem(&1, 0))
  end

  defp get_ready_tasks(tasks) do
    tasks
    |> Enum.filter(fn {_task, deps} -> MapSet.size(deps) == 0 end)
    |> Enum.map(fn {task, _deps} -> task end)
    |> Enum.sort()
  end

  defp get_tasks_not_in_progress(ready, workers) do
    ready
    |> Enum.reject(fn task ->
      Enum.any?(workers, fn {_pid, state} ->
        case state do
          {:busy, ^task} -> true
          _ -> false
        end
      end)
    end)
  end

  defp execute(tasks, workers, base_time, time_elapsed \\ 0) do
    idle = find_idle_workers(workers)
    ready = get_ready_tasks(tasks)

    # filter out the tasks that are already in progress via some workers
    tasks_available = get_tasks_not_in_progress(ready, workers)

    assign_tasks(tasks_available, idle, base_time)

    workers = tick(workers)

    # remove the completed tasks
    tasks = remove_completed(tasks, workers)

    # set the status for the workers who will be finishing their task
    # at the end of the current cycle, to :idle
    # these workers will have returned {:done, name} as their state
    workers =
      Enum.map(workers, fn {worker, state} ->
        case state do
          {:done, _finished_task} -> {worker, :idle}
          _ -> {worker, state}
        end
      end)

    time_elapsed = time_elapsed + 1

    if Map.size(tasks) == 0 do
      time_elapsed
    else
      execute(tasks, workers, base_time, time_elapsed)
    end
  end

  defp remove_completed(tasks, workers) do
    just_completed =
      workers
      |> Enum.filter(fn {_pid, state} ->
        case state do
          {:done, _name} -> true
          _ -> false
        end
      end)
      |> Enum.map(fn {_pid, {:done, name}} -> name end)

    Enum.reduce(just_completed, tasks, fn done_task_name, relations ->
      relations
      |> Enum.map(fn {name, deps} -> {name, MapSet.delete(deps, done_task_name)} end)
      |> Map.new()
      |> Map.delete(done_task_name)
    end)
  end

  defp tick(workers) do
    me = self()

    workers
    |> Enum.map(fn {pid, _state} ->
      send(pid, {:tick, me})

      receive do
        {:tock, state} ->
          {pid, state}
      end
    end)
  end

  defp create_workers(n_workers) do
    for _i <- 1..n_workers do
      worker = spawn(fn -> worker(nil, 0) end)
      {worker, :idle}
    end
  end

  defp worker(name, time_remaining) do
    receive do
      {:schedule_task, {name, time}} ->
        worker(name, time)

      {:tick, sender} ->
        case time_remaining do
          0 ->
            send(sender, {:tock, :idle})
            worker(name, time_remaining)

          1 ->
            send(sender, {:tock, {:done, name}})
            worker(nil, 0)

          _ ->
            send(sender, {:tock, {:busy, name}})
            worker(name, time_remaining - 1)
        end
    end
  end

  def next_step(dependency_map) do
    next =
      dependency_map
      |> Stream.filter(fn {_step, dependencies} -> MapSet.size(dependencies) == 0 end)
      |> Stream.map(fn {step, _dependencies} -> step end)
      |> Enum.sort()
      |> List.first()

    next
  end

  defp find_order(dependency_map, steps) when map_size(dependency_map) == 0 do
    Enum.reverse(steps)
  end

  defp find_order(dependency_map, steps) do
    next = next_step(dependency_map)

    updated_dependency_map =
      dependency_map
      |> Enum.map(fn {step, dependencies} -> {step, MapSet.delete(dependencies, next)} end)
      |> Enum.reject(fn {step, _dependencies} -> step == next end)
      |> Map.new()

    find_order(updated_dependency_map, [next | steps])
  end

  defp starting_steps(dependency_map) do
    dependents =
      dependency_map
      |> Map.keys()
      |> MapSet.new()

    all_points =
      dependency_map
      |> Map.values()
      |> Enum.reduce(MapSet.new(), &MapSet.union(&1, &2))

    MapSet.difference(all_points, dependents)
    |> MapSet.to_list()
  end

  defp input_lines() do
    File.read!("input/day_7.txt") |> String.split("\n", trim: true)
  end

  defp generate_dependency_map() do
    dependency_map =
      input_lines()
      |> Enum.map(&parse_step/1)
      |> Enum.reduce(%{}, fn {step, dependency}, dependency_map ->
        Map.update(
          dependency_map,
          step,
          MapSet.new([dependency]),
          &MapSet.put(&1, dependency)
        )
      end)

    add_starts(dependency_map)
  end

  defp add_starts(dependency_map) do
    start_records =
      dependency_map
      |> starting_steps()
      |> Enum.map(&{&1, MapSet.new()})
      |> Enum.into(%{})

    Map.merge(dependency_map, start_records)
  end

  defp parse_step(
         "Step " <>
           <<dependency::binary-1>> <>
           " must be finished before step " <> <<step::binary-1>> <> " can begin."
       ) do
    {step, dependency}
  end

  def generate_test_dependency_map() do
    %{}
    |> Map.put("A", MapSet.new(["C"]))
    |> Map.put("B", MapSet.new(["A"]))
    |> Map.put("C", MapSet.new())
    |> Map.put("D", MapSet.new(["A"]))
    |> Map.put("E", MapSet.new(["B", "D", "F"]))
    |> Map.put("F", MapSet.new(["C"]))
  end
end
