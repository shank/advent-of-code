defmodule Aoc2018.Day5 do
  @type polymer :: String.t()

  #   ["aA", "bB", "cC", "dD", "eE", "fF", "gG", "hH", "iI", "jJ", "kK", "lL", "mM",
  #  "nN", "oO", "pP", "qQ", "rR", "sS", "tT", "uU", "vV", "wW", "xX", "yY", "zZ",
  #  "Aa", "Bb", "Cc", "Dd", "Ee", "Ff", "Gg", "Hh", "Ii", "Jj", "Kk", "Ll", "Mm",
  #  "Nn", "Oo", "Pp", "Qq", "Rr", "Ss", "Tt", "Uu", "Vv", "Ww", "Xx", "Yy", "Zz"]
  @reacting_unit_pairs (fn ->
                          lower_upper =
                            Enum.zip(?a..?z, ?A..?Z)
                            |> Enum.map(fn {lower, upper} -> to_string([lower, upper]) end)

                          upper_lower =
                            Enum.zip(?A..?Z, ?a..?z)
                            |> Enum.map(fn {upper, lower} -> to_string([upper, lower]) end)

                          lower_upper ++ upper_lower
                        end).()

  # [
  #   ["A", "a"],
  #   ["B", "b"],
  #   ["C", "c"],
  #   ["D", "d"],
  #   ["E", "e"],
  #   ["F", "f"],
  #   ["G", "g"],
  #   ["H", "h"],
  #   ["I", "i"],
  #   ["J", "j"],
  #   ["K", "k"],
  #   ["L", "l"],
  #   ["M", "m"],
  #   ["N", "n"],
  #   ["O", "o"],
  #   ["P", "p"],
  #   ["Q", "q"],
  #   ["R", "r"],
  #   ["S", "s"],
  #   ["T", "t"],
  #   ["U", "u"],
  #   ["V", "v"],
  #   ["W", "w"],
  #   ["X", "x"],
  #   ["Y", "y"],
  #   ["Z", "z"]
  # ]
  @unit_pairs (fn ->
                 @reacting_unit_pairs
                 |> Enum.reduce(MapSet.new(), fn pair, acc ->
                   parts = pair |> String.codepoints() |> Enum.sort()
                   MapSet.put(acc, parts)
                 end)
                 |> MapSet.to_list()
               end).()

  @spec input() :: String.t()
  def input() do
    File.read!("input/day_5.txt")
    |> String.trim()
  end

  @spec main() :: :ok
  def main() do
    input()
    |> part1()
    |> IO.puts()

    input()
    |> part2()
    |> IO.puts()
  end

  @spec part1(polymer) :: non_neg_integer
  def part1(polymer) do
    # make it work
    # make it beautiful
    # make it fast

    # Check if there are any pairs in the polymer
    # if so, remove any pairs from the polymer
    # if not, return the number of characters left in the polymer
    # repeat
    polymer
    |> remove_pairs()
    |> String.length()
  end

  @spec part2(polymer) :: non_neg_integer
  def part2(polymer) do
    # Iterate over each possible unit pairs
    # for each pair compute the length of the polymer after removing those units
    # find the max
    @unit_pairs
    |> Enum.map(fn pair ->
      polymer
      |> String.replace(pair, "")
      |> remove_pairs()
      |> String.length()
    end)
    |> Enum.min()
  end

  @spec remove_pairs(polymer) :: polymer
  def remove_pairs(polymer) do
    if Enum.any?(@reacting_unit_pairs, fn pair -> String.contains?(polymer, pair) end) do
      polymer |> String.replace(@reacting_unit_pairs, "") |> remove_pairs
    else
      polymer
    end
  end
end
