defmodule Aoc2018.Day8 do
  defmodule TreeNode do
    defstruct n_children: 0, n_metadata: 0, children: [], metadata: []
  end

  # test input
  # 2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2

  defp parse_input() do
    File.read!("input/day_8.txt")
    |> String.trim()
    |> String.split(" ", trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  def main() do
    data = parse_input()

    data |> part1() |> IO.puts()
    data |> part2() |> IO.puts()
  end

  defp part2(data) do
    data
    |> init_tree
    |> value()
  end

  defp part1(data) do
    data
    |> init_tree
    |> metadata_sum()
  end

  defp value(%TreeNode{n_children: 0} = node), do: Enum.sum(node.metadata)

  defp value(node) do
    Enum.reduce(node.metadata, 0, fn id, s ->
      if id <= node.n_children do
        child = Enum.at(node.children, id - 1)
        s + value(child)
      else
        s
      end
    end)
  end

  defp metadata_sum(node) do
    Enum.reduce(node.children, Enum.sum(node.metadata), fn child_node, s ->
      s + metadata_sum(child_node)
    end)
  end

  # data :: [integer]
  defp init_tree(data) do
    {node, []} = create_node(data)
    node
  end

  defp create_node([n_children, n_metadata | balance]) do
    node = %TreeNode{n_children: n_children, n_metadata: n_metadata}

    {children, balance} = get_children(balance, n_children)
    {metadata, balance} = get_metadata(balance, n_metadata)

    node = %{node | children: Enum.reverse(children), metadata: metadata}

    {node, balance}
  end

  defp get_children(balance, 0), do: {[], balance}

  defp get_children(balance, n_children) do
    Enum.reduce(1..n_children, {[], balance}, fn _i, {children, balance} ->
      {node, balance} = create_node(balance)
      {[node | children], balance}
    end)
  end

  defp get_metadata(balance, 0), do: {[], balance}
  defp get_metadata(balance, n_metadata), do: Enum.split(balance, n_metadata)
end
