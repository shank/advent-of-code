My solutions to [Advent of Code](https://adventofcode.com).

- [2020](https://gitlab.com/shank/advent-of-code/-/tree/master/aoc_2020) (using [Rust](https://rust-lang.org))
- [2018](https://gitlab.com/shank/advent-of-code/-/tree/master/aoc_2018) (using [Elixir](https://elixir-lang.org))